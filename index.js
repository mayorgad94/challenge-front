'use strict'

const express = require('express')
const hbs = require('express-handlebars')

const app = express()
const port = process.env.PORT || 3001

app.engine('.hbs', hbs({
    defaultLayout: 'default',
    extname: '.hbs'
}))
app.set('view engine', '.hbs')

app.get('/', (req,res) =>{
    res.render('index')
})

app.listen(port, () => {
    console.log(`API REST corriendo en http://localhost:${port}`);
})